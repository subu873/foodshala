<?php require '../../conf/session-check-restaurant.php' ?>
<?php require '../../conf/config.php'; ?>


<!DOCTYPE html>
<html lang="en">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta name="author" content="ChitrakootWeb"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="Onepage Restaurant and Cafe Template"/>
    <meta name="description" content="Onepage Restaurant and Cafe Template"/>

    <!-- title  -->
    <title>FoodShala</title>

    <!-- favicon -->
    <link rel="shortcut icon" href="<?php echo $webroot ?>/template/img/favicon.png">
    <link rel="apple-touch-icon" href="<?php echo $webroot ?>/template/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $webroot ?>/template/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo $webroot ?>/template/img/apple-touch-icon-114x114.png">

    <!-- plugins -->
    <link rel="stylesheet" href="<?php echo $webroot ?>/template/css/plugins.css"/>

    <!-- core style css -->
    <link rel="stylesheet" href="<?php echo $webroot ?>/template/css/style.css"/>

    <script type="text/javascript" src="<?php echo $webroot ?>/assets/js/config.js"></script>

</head>

<body>

<!-- start page loading -->
<div id="preloader">
    <div class="row loader">
        <div class="loader-icon"></div>
    </div>
</div>
<!-- end page loading -->

<!-- start header -->
<?php require '../../layout/header.php' ?>

<section class="page-title-section2 bg-img cover-background" data-overlay-dark="0"
         data-background="https://s3-ap-southeast-1.amazonaws.com/internshala-uploads/banner-images/home/main-1366.png"
         style="background-image: url('https://s3-ap-southeast-1.amazonaws.com/internshala-uploads/banner-images/home/main-1366.png');">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-25">
                <h1 class="col-md-12 title-font font-size28 text-theme-color text-center mt-25">
                    <?php echo ucwords($_SESSION['user_name']) ?> Restaurant
                    <br/>
                    <span>
                        Restaurant Id :
                    </span>
                    <?php echo "FoodShala000" . $_SESSION['user_id'] ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="position-relative bg-white">

    <div class="container">

        <div class="row">
            <div class="col-lg-8 offset-lg-2">

                <div class="padding-70px-right md-padding-50px-right sm-padding-30px-right xs-no-padding-right">

                    <div class="section-heading left half xs-text-center xs-margin-30px-bottom">
                        <div class="title-font font-size22 text-theme-color">
                            <?php echo ucwords($_SESSION['user_name']) ?> Restaurant
                        </div>
                        <h4 class="text-black opacity9">Add New Menu Item</h4>
                    </div>

                    <form method="post" onsubmit="return false" id="addItemForm">

                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" name="name" id="name" placeholder="Please Enter Item Name">
                            </div>

                            <div class="col-md-12">
                                <textarea name="message" rows="3" id="description"
                                          placeholder="Please Enter Item Description"
                                          data-constraints="@NotEmpty"></textarea>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select required name="type" id="type">
                                            <option value="" disabled selected> Select Veg or NonVeg</option>
                                            <option value="veg">Veg</option>
                                            <option value="nonveg">Non-Veg</option>
                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <select required name="type" id="foodCat">
                                            <option value="" disabled selected>Select Food Category</option>
                                            <option value="special">Special</option>
                                            <option value="breakfast">Breakfast</option>
                                            <option value="lunch">Lunch</option>
                                            <option value="dinner">Dinner</option>
                                            <option value="snacks">Snacks</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                               <textarea name="message" id="ingredients" rows="3"
                                         placeholder="Item Ingredients (Ex: Cucumbers and Tomatoes)"
                                         data-constraints="@NotEmpty"></textarea>
                            </div>

                            <div class="col-md-12">
                                <input type="number" name="price" id="price" placeholder="Please Enter Price in INR">
                            </div>

                            <div class="col-md-12">

                                <span class="text-success font-weight-700" id="successItemMsg"></span>
                                <span class="text-danger font-weight-700" id="errorItemMsg"></span>

                            </div>


                            <div class="mfControls col-md-12" style="margin-top: 15px">
                                <button class="btn book-now" id="addItem">Add Item In Menu</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>



</section>

<?php require '../../layout/footer.php' ?>


<!-- jQuery -->
<script src="<?php echo $webroot ?>/template/js/jquery.min.js"></script>

<!-- popper.min -->
<script src="<?php echo $webroot ?>/template/js/popper.min.js"></script>

<!-- bootstrap -->
<script src="<?php echo $webroot ?>/template/js/bootstrap.min.js"></script>

<!-- scrollIt -->
<script src="<?php echo $webroot ?>/template/js/scrollIt.min.js"></script>

<!-- stellar js -->
<script src="<?php echo $webroot ?>/template/js/jquery.stellar.min.js"></script>

<!-- isotope.pkgd.min js -->
<script src="<?php echo $webroot ?>/template/js/isotope.pkgd.min.js"></script>


<!-- tab -->
<script src="<?php echo $webroot ?>/template/js/easy.responsive.tabs.js"></script>


<!-- custom scripts -->
<script src="<?php echo $webroot ?>/template/js/main.js"></script>

<script type="text/javascript" src="<?php echo $webroot ?>/assets/js/add-item.js"></script>



