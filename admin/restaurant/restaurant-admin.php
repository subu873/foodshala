<?php require '../../conf/session-check-restaurant.php' ?>
<?php require '../../conf/config.php'; ?>
<?php
require '../../conf/db.php'; // db config file
require '../../server/FoodShala.php'; // Foodshala class file
$foodShala = new FoodShala($conn);
$user = $_SESSION['user_id'];

$breakfast = $foodShala->getMenuList('breakfast', $user);
$lunch = $foodShala->getMenuList('lunch', $user);
$dinner = $foodShala->getMenuList('dinner', $user);
$special = $foodShala->getMenuList('special', $user);
$snacks = $foodShala->getMenuList('snacks', $user);

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta name="author" content="ChitrakootWeb"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="Onepage Restaurant and Cafe Template"/>
    <meta name="description" content="Onepage Restaurant and Cafe Template"/>

    <!-- title  -->
    <title>FoodShala</title>

    <!-- favicon -->
    <link rel="shortcut icon" href="<?php echo $webroot ?>/template/img/favicon.png">
    <link rel="apple-touch-icon" href="<?php echo $webroot ?>/template/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $webroot ?>/template/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo $webroot ?>/template/img/apple-touch-icon-114x114.png">

    <!-- plugins -->
    <link rel="stylesheet" href="<?php echo $webroot ?>/template/css/plugins.css"/>

    <!-- core style css -->
    <link rel="stylesheet" href="<?php echo $webroot ?>/template/css/style.css"/>


    <!-- jQuery -->
    <script src="<?php echo $webroot ?>/template/js/jquery.min.js"></script>

    <!-- popper.min -->
    <script src="<?php echo $webroot ?>/template/js/popper.min.js"></script>

    <!-- bootstrap -->
    <script src="<?php echo $webroot ?>/template/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?php echo $webroot ?>/assets/js/config.js"></script>

</head>

<body>

<!-- start page loading -->
<div id="preloader">
    <div class="row loader">
        <div class="loader-icon"></div>
    </div>
</div>
<!-- end page loading -->

<!-- start header -->
<?php require '../../layout/header.php' ?>

<section class="page-title-section2 bg-img cover-background" data-overlay-dark="0"
         data-background="https://s3-ap-southeast-1.amazonaws.com/internshala-uploads/banner-images/home/main-1366.png"
         style="background-image: url('https://s3-ap-southeast-1.amazonaws.com/internshala-uploads/banner-images/home/main-1366.png');">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="col-md-12 title-font font-size22 text-theme-color text-center">
                    <?php echo ucwords($_SESSION['user_name']) ?> Restaurant
                    <br/>
                    <span>
                        Restaurant Id :
                    </span>
                    <?php echo "FoodShala000" . $_SESSION['user_id'] ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="position-relative" data-scroll-index="2" data-overlay-dark="0" data-background="img/bg-section.jpg"
         style="background-image: url(&quot;img/bg-section.jpg&quot;);">
    <div class="container">


        <div class="section-heading">
            <div class="title-font font-size22 text-theme-color">Discover our popular dishes</div>
            <h3 class="font-size24 text-uppercase">Our Menu</h3>
        </div>

        <div class="col-md-12 text-center mb-25">

            <a class="btn btn-primary mb-25" href="<?php echo $webroot ?>/admin/restaurant/add-menu-items.php"> Click
                here to add Items in Menu</a>

        </div>

        <div class="horizontaltab tab-style" style="display: block; width: 100%; margin: 0px;">
            <ul class="resp-tabs-list hor_1">
                <li class="resp-tab-item hor_1 resp-tab-active" aria-controls="hor_1_tab_item-0" role="tab">
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-01.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Special</span>
                </li>
                <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-1" role="tab">
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-02.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Breakfast</span>
                </li>
                <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-2" role="tab">
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-03.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Lunch</span>
                </li>
                <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-3" role="tab">
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-04.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Dinner</span>
                </li>

                <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-4" role="tab">
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-05.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Snacks</span>
                </li>
            </ul>
            <div class="resp-tabs-container hor_1">

                <h2 class="resp-accordion hor_1 resp-tab-active" role="tab" aria-controls="hor_1_tab_item-0"
                    style="background: none;"><span class="resp-arrow"></span>
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-01.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Special</span>
                </h2>
                <div class="resp-tab-content hor_1 resp-tab-content-active" aria-labelledby="hor_1_tab_item-0"
                     style="display:block">

                    <div class="row">
                        <?php foreach ($special as $key => $value) { ?>

                            <div class="col-md-6 margin-30px-bottom sm-margin-20px-bottom">
                                <div class="border position-relative padding-20px-all bg-white padding-70px-right md-padding-60px-right">
                                    <div class="alt-font font-weight-700 margin-8px-bottom xs-margin-5px-bottom">
                                        <?php echo $value['name'] ?>
                                    </div>
                                    <p class="width-95 xs-width-100">
                                        <?php echo $value['description'] ?>
                                    </p>
                                    <div class="price-label bg-medium-light-gray text-theme-color alt-font font-weight-600">
                                        <span> ₹ <?php echo $value['price'] ?></span></div>
                                    <ul class="no-margin text-uppercase font-size12">
                                        <li>
                                            <span class="font-weight-700 alt-font">Ingredients</span>
                                            <span class="border-left padding-10px-left margin-10px-left"> <?php echo $value['ingredients'] ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        <?php } ?>
                    </div>

                </div>



                <h2 class="resp-accordion hor_1" role="tab" aria-controls="hor_1_tab_item-1">
                    <span class="resp-arrow"></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Breakfast</span>
                </h2>

                <div class="resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-1">
                    <div class="row">
                        <?php foreach ($breakfast as $key => $value) { ?>

                            <div class="col-md-6 margin-30px-bottom sm-margin-20px-bottom">
                                <div class="border position-relative padding-20px-all bg-white padding-70px-right md-padding-60px-right">
                                    <div class="alt-font font-weight-700 margin-8px-bottom xs-margin-5px-bottom">
                                        <?php echo $value['name'] ?>
                                    </div>
                                    <p class="width-95 xs-width-100">
                                        <?php echo $value['description'] ?>
                                    </p>
                                    <div class="price-label bg-medium-light-gray text-theme-color alt-font font-weight-600">
                                        <span> ₹ <?php echo $value['price'] ?></span></div>
                                    <ul class="no-margin text-uppercase font-size12">
                                        <li>
                                            <span class="font-weight-700 alt-font">Ingredients</span>
                                            <span class="border-left padding-10px-left margin-10px-left"> <?php echo $value['ingredients'] ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                       <?php } ?>
                    </div>
                </div>


                <h2 class="resp-accordion hor_1" role="tab" aria-controls="hor_1_tab_item-2"><span
                            class="resp-arrow"></span>
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-03.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Lunch</span>
                </h2>
                <div class="resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-2">

                    <div class="row">
                        <?php foreach ($lunch as $key => $value) { ?>

                            <div class="col-md-6 margin-30px-bottom sm-margin-20px-bottom">
                                <div class="border position-relative padding-20px-all bg-white padding-70px-right md-padding-60px-right">
                                    <div class="alt-font font-weight-700 margin-8px-bottom xs-margin-5px-bottom">
                                        <?php echo $value['name'] ?>
                                    </div>
                                    <p class="width-95 xs-width-100">
                                        <?php echo $value['description'] ?>
                                    </p>
                                    <div class="price-label bg-medium-light-gray text-theme-color alt-font font-weight-600">
                                        <span> ₹ <?php echo $value['price'] ?></span></div>
                                    <ul class="no-margin text-uppercase font-size12">
                                        <li>
                                            <span class="font-weight-700 alt-font">Ingredients</span>
                                            <span class="border-left padding-10px-left margin-10px-left"> <?php echo $value['ingredients'] ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        <?php } ?>
                    </div>

                </div>
                <h2 class="resp-accordion hor_1" role="tab" aria-controls="hor_1_tab_item-3"><span
                            class="resp-arrow"></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Dinner</span>
                </h2>
                <div class="resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-3">

                    <div class="row">
                        <?php foreach ($dinner as $key => $value) { ?>

                            <div class="col-md-6 margin-30px-bottom sm-margin-20px-bottom">
                                <div class="border position-relative padding-20px-all bg-white padding-70px-right md-padding-60px-right">
                                    <div class="alt-font font-weight-700 margin-8px-bottom xs-margin-5px-bottom">
                                        <?php echo $value['name'] ?>
                                    </div>
                                    <p class="width-95 xs-width-100">
                                        <?php echo $value['description'] ?>
                                    </p>
                                    <div class="price-label bg-medium-light-gray text-theme-color alt-font font-weight-600">
                                        <span> ₹ <?php echo $value['price'] ?></span></div>
                                    <ul class="no-margin text-uppercase font-size12">
                                        <li>
                                            <span class="font-weight-700 alt-font">Ingredients</span>
                                            <span class="border-left padding-10px-left margin-10px-left"> <?php echo $value['ingredients'] ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        <?php } ?>
                    </div>

                </div>


                <h2 class="resp-accordion hor_1" role="tab" aria-controls="hor_1_tab_item-4"><span
                            class="resp-arrow"></span>

                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Snacks</span>
                </h2>
                <div class="resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-4">

                    <div class="row">
                        <?php foreach ($snacks as $key => $value) { ?>

                            <div class="col-md-6 margin-30px-bottom sm-margin-20px-bottom">
                                <div class="border position-relative padding-20px-all bg-white padding-70px-right md-padding-60px-right">
                                    <div class="alt-font font-weight-700 margin-8px-bottom xs-margin-5px-bottom">
                                        <?php echo $value['name'] ?>
                                    </div>
                                    <p class="width-95 xs-width-100">
                                        <?php echo $value['description'] ?>
                                    </p>
                                    <div class="price-label bg-medium-light-gray text-theme-color alt-font font-weight-600">
                                        <span> ₹ <?php echo $value['price'] ?></span></div>
                                    <ul class="no-margin text-uppercase font-size12">
                                        <li>
                                            <span class="font-weight-700 alt-font">Ingredients</span>
                                            <span class="border-left padding-10px-left margin-10px-left"> <?php echo $value['ingredients'] ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

</section>

<?php require '../../layout/footer.php' ?>



<!-- scrollIt -->
<script src="<?php echo $webroot ?>/template/js/scrollIt.min.js"></script>

<!-- stellar js -->
<script src="<?php echo $webroot ?>/template/js/jquery.stellar.min.js"></script>

<!-- isotope.pkgd.min js -->
<script src="<?php echo $webroot ?>/template/js/isotope.pkgd.min.js"></script>


<!-- tab -->
<script src="<?php echo $webroot ?>/template/js/easy.responsive.tabs.js"></script>


<!-- custom scripts -->
<script src="<?php echo $webroot ?>/template/js/main.js"></script>





