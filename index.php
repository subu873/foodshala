<?php session_start();
?>
<?php require 'conf/config.php';
require 'conf/db.php'; // db config file
require 'server/FoodShala.php'; // Foodshala class file
$foodShala = new FoodShala($conn);

$breakfast = $foodShala->getMenuListWithRestaurantName('breakfast');
$lunch = $foodShala->getMenuListWithRestaurantName('lunch');
$dinner = $foodShala->getMenuListWithRestaurantName('dinner');
$special = $foodShala->getMenuListWithRestaurantName('special');
$snacks = $foodShala->getMenuListWithRestaurantName('snacks');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta name="author" content="ChitrakootWeb"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="Onepage Restaurant and Cafe Template"/>
    <meta name="description" content="Onepage Restaurant and Cafe Template"/>

    <!-- title  -->
    <title>FoodShala</title>

    <!-- favicon -->
    <link rel="shortcut icon" href="<?php echo $webroot ?>/template/img/favicon.png">
    <link rel="apple-touch-icon" href="<?php echo $webroot ?>/template/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $webroot ?>/template/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo $webroot ?>/template/img/apple-touch-icon-114x114.png">

    <!-- plugins -->
    <link rel="stylesheet" href="<?php echo $webroot ?>/template/css/plugins.css"/>

    <!-- core style css -->
    <link rel="stylesheet" href="<?php echo $webroot ?>/template/css/style.css"/>


    <script src="<?php echo $webroot ?>/template/js/jquery.min.js"></script>

    <script type="text/javascript" src="<?php echo $webroot ?>/assets/js/config.js"></script>

</head>

<body>

<?php include 'conf/user-data.php' ?>
<!-- start page loading -->
<div id="preloader">
    <div class="row loader">
        <div class="loader-icon"></div>
    </div>
</div>
<!-- end page loading -->

<!-- start header -->
<?php require 'layout/header.php' ?>
<!-- end header -->

<!-- start about section -->
<section data-scroll-index="1" data-overlay-dark="0" data-background="img/bg-section.jpg">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-md-12 display-table  sm-margin-30px-bottom xs-no-margin-top">

                <div class="section-heading left half xs-margin-20px-bottom">
                    <div class="title-font">Unique story</div>
                    <h4>About us</h4>
                </div>

                <p>Restaurant food dolor sit amet, consectetur adipisicing elit, sed do eiusmod eius modi tempora
                    incidunt ut labore et dolore magnam aliquam quaerat eius modi tempora incidunt ut.</p>
                <p>Cafe ut perspiciatis unde omnis iste natus error sit voluptatem totam rem aperiam, eaque ipsa quae ab
                    illo inventore veritatis dicta sunt explicabo.</p>

                <a href="javascript:void(0);" data-scroll-nav="4" class="btn">Book Now</a>

            </div>

            <div class="col-lg-5 col-md-9 col-xs-12 offset-lg-1 offset-md-0 md-margin-five-top sm-no-margin-top text-center display-table">
                <div class="display-table-cell vertical-align-middle">
                    <img src="img/content-02.jpg" alt="" class="width-50 float-left padding-5px-right border-radius-5">
                    <img src="img/content-01.jpg" alt="" class="width-50 float-left padding-5px-left border-radius-5">
                </div>
            </div>

        </div>
    </div>
    <div class="corner-img about-corner sm-display-none wow fadeInLeft" data-wow-duration="0.8s">
        <img src="img/delicious-bg.png" alt=""/>
    </div>
</section>
<!-- end about section -->


<!-- start tab section -->
<?php include 'menu-list-homepage.php' ?>
<!-- end tab section -->

<!--  start delicious section -->
<section class="bg-img cover-background" data-overlay-dark="3" data-background="img/delicious-back-img.png">

    <div class="container">

        <div class="section-heading">
            <div class="title-font">Amazing</div>
            <h3 class="text-white opacity9">Delicious</h3>
        </div>

        <div class="delicious-menu">
            <div class="owl-carousel owl-theme">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="border border-width-8 width-100 border-color-light-white">
                            <img src="img/drink.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="block border border-width-8 width-100 border-color-light-black padding-25px-top padding-30px-bottom md-padding-15px-tb padding-40px-lr md-padding-30px-lr xs-padding-20px-lr margin-thirteen-top md-margin-six-top">
                            <div class="title-font margin-15px-bottom">Drinks</div>

                            <ul class="menu-row">
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Soft Drink</p>
                                    <span class="price">$8</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Strawberry Lemonade</p>
                                    <span class="price">$14</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Chicken Clear Soup</p>
                                    <span class="price">$27</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Veg Hot N Sour Soup</p>
                                    <span class="price">$33</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Chicken & Sausage Gumbo</p>
                                    <span class="price">$16</span>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-7">
                        <div class="border border-width-8 width-100 border-color-light-white">
                            <img src="img/main-course.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="block border border-width-8 width-100 border-color-light-black padding-25px-top padding-30px-bottom md-padding-15px-tb padding-40px-lr md-padding-30px-lr xs-padding-20px-lr margin-thirteen-top md-margin-six-top">
                            <div class="title-font margin-15px-bottom">Main Course</div>

                            <ul class="menu-row">
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Fish Tikka Masala</p>
                                    <span class="price">$24</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Tuna Salad Sandwich</p>
                                    <span class="price">$215</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Lightly Fried Shrimp</p>
                                    <span class="price">$134</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Basil Roasted Chicken</p>
                                    <span class="price">$389</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Mutton Food Inn Special</p>
                                    <span class="price">$877</span>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-7">
                        <div class="border border-width-8 width-100 border-color-light-white">
                            <img src="img/desserts.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="block border border-width-8 width-100 border-color-light-black padding-25px-top padding-30px-bottom md-padding-15px-tb padding-40px-lr md-padding-30px-lr xs-padding-20px-lr margin-thirteen-top md-margin-six-top">
                            <div class="title-font margin-15px-bottom">Desserts</div>

                            <ul class="menu-row">
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Mini Cupcakes</p>
                                    <span class="price">$32</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Whole Pie Or Cake</p>
                                    <span class="price">$90</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Fruit with Ice Cream</p>
                                    <span class="price">$42</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">Chocolate Chip Cookies</p>
                                    <span class="price">$25</span>
                                </li>
                                <li>
                                    <div class="menu-item"><img src="img/dish.png" alt=""/></div>
                                    <p class="item-name">White Chocolate Bread Pudding</p>
                                    <span class="price">$29</span>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</section>


<!-- end delicious section -->

<!-- start team section -->
<section class="chef-section" data-scroll-index="3" data-overlay-dark="0" data-background="img/bg-section.jpg">
    <div class="container">

        <div class="section-heading">
            <div class="title-font">Our expert</div>
            <h3>Meet Our Chef</h3>
        </div>

        <div class="owl-carousel owl-theme">

            <div class="bg-white border-width-5 border border-color-light-black height-100">
                <div><img alt="img" src="img/team/chef-02.jpg"></div>
                <div class="padding-30px-all sm-padding-25px-all">
                    <h5 class="font-size20 sm-font-size18 xs-font-size16 margin-5px-bottom">Agnesi Gruber</h5>
                    <h6 class="font-size11 text-uppercase text-theme-color no-margin-bottom">executive chef</h6>
                    <div class="separator-line-verticle-small-thick bg-theme margin-15px-top xs-margin-10px-top width-15 margin-20px-bottom"></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et.</p>
                    <span><img src="img/chef_sign.png" alt=""></span>
                </div>
            </div>

            <div class="bg-white border-width-5 border border-color-light-black height-100">
                <div><img alt="img" src="img/team/chef-01.jpg"></div>
                <div class="padding-30px-all sm-padding-25px-all">
                    <h5 class="font-size20 sm-font-size18 xs-font-size16 margin-5px-bottom">Frances Karle</h5>
                    <h6 class="font-size11 text-uppercase text-theme-color no-margin-bottom">executive chef</h6>
                    <div class="separator-line-verticle-small-thick bg-theme margin-15px-top xs-margin-10px-top width-15 margin-20px-bottom"></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et.</p>
                    <span><img src="img/chef_sign.png" alt=""></span>

                </div>
            </div>

            <div class="bg-white border-width-5 border border-color-light-black height-100">
                <div><img alt="img" src="img/team/chef-03.jpg"></div>
                <div class="padding-30px-all sm-padding-25px-all">
                    <h5 class="font-size20 sm-font-size18 xs-font-size16 margin-5px-bottom">Raule Staley</h5>
                    <h6 class="font-size11 text-uppercase text-theme-color no-margin-bottom">executive chef</h6>
                    <div class="separator-line-verticle-small-thick bg-theme margin-15px-top xs-margin-10px-top width-15 margin-20px-bottom"></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et.</p>
                    <span><img src="img/chef_sign.png" alt=""></span>

                </div>
            </div>
        </div>

    </div>
</section>
<!-- end team section -->

<!-- start book-table section -->
<section class="position-relative bg-black" data-scroll-index="4" data-overlay-dark="0"
         data-background="img/black_twill.png">

    <div class="container">

        <div class="row">
            <div class="col-md-6">

                <div class="padding-70px-right md-padding-50px-right sm-padding-30px-right xs-no-padding-right">

                    <div class="section-heading left half xs-text-center xs-margin-30px-bottom">
                        <div class="title-font font-size22 text-theme-color">Reservation</div>
                        <h4 class="text-white opacity9">Book a Table</h4>
                    </div>

                    <form method="post" action="../../bat/rd-mailform.php" class="mailform off2">
                        <input type="hidden" name="form-type" value="contact">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" name="name" placeholder="Your Name:"
                                       data-constraints="@LettersOnly @NotEmpty">
                            </div>

                            <div class="col-md-12">
                                <input type="text" name="email" placeholder="Email:"
                                       data-constraints="@Email @NotEmpty">
                            </div>

                            <div class="col-md-12">

                                <div class="row">
                                    <div class="col-md-6 no-padding-right xs-padding-15px-right">
                                        <input type="text" name="phone" placeholder="Telephone:"
                                               data-constraints="@Phone">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="persons" placeholder="Persons:"
                                               data-constraints="@NumbersOnly @NotEmpty">
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12">

                                <div class="row">
                                    <div class="col-md-6 no-padding-right xs-padding-15px-right">
                                        <input type="text" name="date" placeholder="Date:" data-provide="datepicker"
                                               data-constraints="@Date @NotEmpty">
                                    </div>
                                    <div class="col-md-6">
                                        <input id="time-input" type="text" name="clockpicker" placeholder="Time"
                                               data-constraints='@NotEmpty'>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12">
                                <textarea name="message" rows="5" placeholder="Message:"
                                          data-constraints="@NotEmpty"></textarea>
                            </div>
                            <div class="mfControls col-md-12">
                                <button type="submit" class="btn book-now">Book Now</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="bg-img cover-background width-50 position-absolute position-right position-top height-100 xs-display-none story-video"
         data-overlay-dark="0" data-background="img/bg-book-now.jpg">

        <!-- start play button -->
        <div class="absolute-middle-center z-index-9">
            <div class="display-inline-block vertical-align-middle">
                <a class="video video_btn" href="<?php echo $webroot ?>/template/../../video/down-time.mp4"><i
                            class="fas fa-play font-size18 xs-font-size16"></i>
                </a>
            </div>
        </div>
        <!-- end play button -->

    </div>

</section>
<!-- end book-table section -->

<!-- start gallery section -->
<section class="portfolio" data-scroll-index="5">
    <div class="container-fluid padding-7px-lr">

        <div class="section-heading">
            <div class="title-font font-size22 text-theme-color">Discover our popular dishes</div>
            <h3>Food Gallery</h3>
        </div>

        <!-- gallery -->
        <div class="row half-gutter xs-default-gutter gallery text-center width-100">

            <!-- start gallery item -->
            <div class="col-lg-3 col-md-4 col-sm-6 items">
                <div class="item-img">
                    <img src="img/portfolio/1.jpg" alt="image">
                    <div class="item-img-overlay valign">
                        <div class="overlay-info width-100 vertical-center">
                            <a href="<?php echo $webroot ?>/template/img/portfolio/1.jpg" class="popimg">
                                <i class="fa fa-search-plus"></i>
                            </a>
                            <h6>Roasted Chicken</h6>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end gallery item -->

            <!-- start gallery item -->
            <div class="col-lg-3 col-md-4 col-sm-6 items">
                <div class="item-img">
                    <img src="img/portfolio/2.jpg" alt="image">
                    <div class="item-img-overlay valign">
                        <div class="overlay-info width-100 vertical-center">
                            <a href="<?php echo $webroot ?>/template/img/portfolio/2.jpg" class="popimg">
                                <i class="fa fa-search-plus"></i>
                            </a>
                            <h6>Fusilli Pasta Salad</h6>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end gallery item -->

            <!-- start gallery item -->
            <div class="col-lg-3 col-md-4 col-sm-6 items">
                <div class="item-img">
                    <img src="img/portfolio/3.jpg" alt="image">
                    <div class="item-img-overlay valign">
                        <div class="overlay-info width-100 vertical-center">
                            <a href="<?php echo $webroot ?>/template/img/portfolio/3.jpg" class="popimg">
                                <i class="fa fa-search-plus"></i>
                            </a>
                            <h6>Delicious Pastry</h6>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end gallery item -->

            <!-- start gallery item -->
            <div class="col-lg-3 col-md-4 col-sm-6 items">
                <div class="item-img">
                    <img src="img/portfolio/4.jpg" alt="image">
                    <div class="item-img-overlay valign">
                        <div class="overlay-info width-100 vertical-center">
                            <a href="<?php echo $webroot ?>/template/img/portfolio/4.jpg" class="popimg">
                                <i class="fa fa-search-plus"></i>
                            </a>
                            <h6>Slice Toast Bread</h6>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end gallery item -->

            <!-- start gallery item -->
            <div class="col-lg-3 col-md-4 col-sm-6 items">
                <div class="item-img">
                    <img src="img/portfolio/5.jpg" alt="image">
                    <div class="item-img-overlay valign">
                        <div class="overlay-info width-100 vertical-center">
                            <a href="<?php echo $webroot ?>/template/img/portfolio/5.jpg" class="popimg">
                                <i class="fa fa-search-plus"></i>
                            </a>
                            <h6>Tuna Salad</h6>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end gallery item -->

            <!-- start gallery item -->
            <div class="col-lg-3 col-md-4 col-sm-6 items">
                <div class="item-img">
                    <img src="img/portfolio/6.jpg" alt="image">
                    <div class="item-img-overlay valign">
                        <div class="overlay-info width-100 vertical-center">
                            <a href="<?php echo $webroot ?>/template/img/portfolio/6.jpg" class="popimg">
                                <i class="fa fa-search-plus"></i>
                            </a>
                            <h6>Grilles Chicken</h6>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end gallery item -->

            <!-- start gallery item -->
            <div class="col-lg-3 col-md-4 col-sm-6 items">
                <div class="item-img">
                    <img src="img/portfolio/7.jpg" alt="image">
                    <div class="item-img-overlay valign">
                        <div class="overlay-info width-100 vertical-center">
                            <a href="<?php echo $webroot ?>/template/img/portfolio/7.jpg" class="popimg">
                                <i class="fa fa-search-plus"></i>
                            </a>
                            <h6>Fried Egg</h6>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end gallery item -->

            <!-- start gallery item -->
            <div class="col-lg-3 col-md-4 col-sm-6 items">
                <div class="item-img">
                    <img src="img/portfolio/8.jpg" alt="image">
                    <div class="item-img-overlay valign">
                        <div class="overlay-info width-100 vertical-center">
                            <a href="<?php echo $webroot ?>/template/img/portfolio/8.jpg" class="popimg">
                                <i class="fa fa-search-plus"></i>
                            </a>
                            <h6>Fresh Drink</h6>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end gallery item -->

            <div class="clear-fix"></div>

        </div>

    </div>

</section>
<!-- end gallery section -->

<!-- start testimonials section -->
<section class="parallax cover-background testimonials" data-overlay-dark="8" data-background="img/bg6.jpg">

    <div class="container">

        <div class="section-heading">
            <div class="title-font font-size22 text-theme-color">Testimonials</div>
            <h3 class="text-white opacity9">What Our Customers Say</h3>
        </div>

        <div class="owl-carousel owl-theme text-center">

            <!-- start testimonials item-->
            <div>
                <i class="fas fa-quote-left font-size22 margin-45px-bottom md-margin-35px-bottom sm-margin-30px-bottom xs-margin-20px-bottom text-light-gray"></i>
                <p class="width-70 xs-width-90 center-col margin-50px-bottom md-margin-40px-bottom sm-margin-30px-bottom xs-margin-25px-bottom font-size16 xs-font-size14 text-light-gray line-height-28 xs-line-height-24">
                    Laudantium, oloremquetotam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                    architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem Nemo enim ipsam voluptatem
                    Nemo enim ipsam voluptatem.</p>
                <img src="img/testmonials/t-1.jpg" class="rounded-circle margin-15px-bottom" alt=""/>
                <h5 class="text-white opacity9 font-size16 no-margin-bottom">Jama Karle</h5>
            </div>
            <!-- end testimonials item-->

            <!-- start testimonials item-->
            <div>
                <i class="fas fa-quote-left font-size22 margin-45px-bottom md-margin-35px-bottom sm-margin-30px-bottom xs-margin-20px-bottom text-light-gray"></i>
                <p class="width-70 xs-width-90 center-col margin-50px-bottom md-margin-40px-bottom sm-margin-30px-bottom xs-margin-25px-bottom font-size16 xs-font-size14 text-light-gray line-height-28 xs-line-height-24">
                    voluptatem doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et
                    quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem Nemo enim ipsam
                    voluptatem Nemo enim ipsam voluptatem.</p>
                <img src="img/testmonials/t-3.jpg " class="rounded-circle margin-15px-bottom " alt=" "/>
                <h5 class="text-white opacity9 font-size16 no-margin-bottom ">Finley Walkeror</h5>
            </div>
            <!-- end testimonials item-->

            <!-- start testimonials item-->
            <div>
                <i class="fas fa-quote-left font-size22 margin-45px-bottom md-margin-35px-bottom sm-margin-30px-bottom xs-margin-20px-bottom text-light-gray"></i>
                <p class="width-70 xs-width-90 center-col margin-50px-bottom md-margin-40px-bottom sm-margin-30px-bottom xs-margin-25px-bottom font-size16 xs-font-size14 text-light-gray line-height-28 xs-line-height-24">
                    Doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
                    architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem Nemo enim ipsam voluptatem
                    Nemo enim ipsam voluptatem.</p>
                <img src="img/testmonials/t-2.jpg" class="rounded-circle margin-15px-bottom" alt=""/>
                <h5 class="text-white opacity9 font-size16 no-margin-bottom">Keir Prestonly</h5>
            </div>
            <!-- end testimonials item-->

        </div>
    </div>

</section>
<!-- end testimonials section -->

<!-- start blog section -->
<section class="blog" data-scroll-index="6" data-overlay-dark="0" data-background="img/bg-section.jpg">
    <div class="container">
        <div class="section-heading">
            <div class="title-font font-size22 text-theme-color">Latest news</div>
            <h3>Our Blog</h3>
        </div>
        <div class="row">

            <!-- start blog -->
            <div class="col-lg-4 sm-margin-20px-bottom">
                <div class="item text-center">
                    <div class="post-img">
                        <img src="img/blog/3.jpg" alt="">
                    </div>
                    <div class="content sm-padding-20px-all">
                        <h6 class="font-size16 sm-font-size15 xs-font-size14"><a
                                    href="<?php echo $webroot ?>/template/blog.html">Best foods for you</a></h6>
                        <div class="author">
                            <span class="font-size12 display-inline-block">by <a href="javascript:void(0);">Jaydev makhansra</a>&nbsp;&nbsp;|&nbsp;&nbsp;02 May 2018</span>
                        </div>
                        <p class="margin-20px-tb sm-margin-15px-tb xs-margin-10px-tb">Lorem ipsum dolor sit amet,
                            consectetur adipisicing elit, sed do eiusmod tempor incididunt. </p>
                        <a href="single-post.html" class="read-more">Read more</a>
                    </div>
                </div>
            </div>
            <!-- end blog -->

            <!-- start blog -->
            <div class="col-lg-4 sm-margin-20px-bottom">
                <div class="item text-center">
                    <div class="post-img">
                        <img src="img/blog/2.jpg" alt="">
                    </div>
                    <div class="content sm-padding-20px-all">

                        <h6 class="font-size16 sm-font-size15 xs-font-size14"><a href="blog.html">Couple having
                                dinner</a></h6>
                        <div class="author">
                            <span class="font-size12 display-inline-block">by <a href="javascript:void(0);">John kairliya</a>&nbsp;&nbsp;|&nbsp;&nbsp;11 April 2018</span>
                        </div>

                        <p class="margin-20px-tb sm-margin-15px-tb xs-margin-10px-tb">Lorem ipsum dolor sit amet,
                            consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                        <a href="<?php echo $webroot ?>/template/single-post.html" class="read-more">Read more</a>
                    </div>
                </div>
            </div>
            <!-- end blog -->

            <!-- start blog -->
            <div class="col-lg-4">
                <div class="item text-center">
                    <div class="post-img">
                        <img src="img/blog/1.jpg" alt="">
                    </div>
                    <div class="content sm-padding-20px-all">
                        <h6 class="font-size16 sm-font-size15 xs-font-size14"><a href="blog.html">Working in the
                                kitchen</a></h6>
                        <div class="author">
                            <span class="font-size12 display-inline-block">by <a
                                        href="javascript:void(0);">Jay Benjamin</a>&nbsp;&nbsp;|&nbsp;&nbsp;04 March 2018</span>
                        </div>
                        <p class="margin-20px-tb sm-margin-15px-tb xs-margin-10px-tb">Lorem ipsum dolor sit amet,
                            consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                        <a href="single-post.html" class="read-more">Read more</a>
                    </div>
                </div>
            </div>
            <!-- end blog -->

        </div>
    </div>
</section>
<!-- end blog section -->

<!--  start catering section -->
<section class="parallax cover-background" data-overlay-dark="8" data-background="img/bg-catering.jpg">
    <div class="container text-center">
        <div class="title-font large display-block text-white margin-30px-bottom sm-margin-25px-bottom xs-margin-20px-bottom">
            We also provide catering services
        </div>
        <p class="text-white opacity9 margin-30px-bottom sm-margin-25px-bottom xs-margin-20px-bottom font-size24 md-font-size22 sm-font-size18 xs-font-size16 line-height-40 sm-line-height-30 xs-line-height-26 width-55 md-width-60 sm-width-70 xs-width-85 center-col font-weight-300">
            We provide best and fresh quality foods. We also gives you required signature dishes.</p>
        <a href="javascript:void(0);" data-scroll-nav="4" class="btn theme">Book Services</a>
    </div>
</section>
<!-- end catering section -->

<!--  start footer section -->
<?php require 'layout/footer.php' ?>
<!-- end footer section -->

<!-- all js include start -->

<!-- jQuery -->

<!-- popper.min -->
<script src="<?php echo $webroot ?>/template/js/popper.min.js"></script>

<!-- bootstrap -->
<script src="<?php echo $webroot ?>/template/js/bootstrap.min.js"></script>

<!-- scrollIt -->
<script src="<?php echo $webroot ?>/template/js/scrollIt.min.js"></script>

<!-- tab -->
<script src="<?php echo $webroot ?>/template/js/easy.responsive.tabs.js"></script>

<!-- owl carousel -->
<script src="<?php echo $webroot ?>/template/js/owl.carousel.min.js"></script>

<!-- jquery.magnific-popup js -->
<script src="<?php echo $webroot ?>/template/js/jquery.magnific-popup.min.js"></script>

<!-- datepicker js -->
<script src="<?php echo $webroot ?>/template/js/datepicker.min.js"></script>

<!-- clockpicker js -->
<script src="<?php echo $webroot ?>/template/js/clockpicker.js"></script>

<!-- stellar js -->
<script src="<?php echo $webroot ?>/template/js/jquery.stellar.min.js"></script>

<!-- isotope.pkgd.min js -->
<script src="<?php echo $webroot ?>/template/js/isotope.pkgd.min.js"></script>

<!-- wow js -->
<script src="<?php echo $webroot ?>/template/js/wow.js"></script>


<!-- custom scripts -->
<script src="<?php echo $webroot ?>/template/js/main.js"></script>


<script type="text/javascript">
    var user = '<?php echo $user ?>';
    var username = '<?php echo $username ?>';
    var role = '<?php echo $role ?>';
</script>

<script type="text/javascript" src="<?php echo $webroot ?>/assets/js/order-food.js"></script>

</body>
</html>