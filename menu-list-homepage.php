<section class="position-relative" data-scroll-index="2" id="MenuItemsList">
    <div class="container">


        <div class="section-heading">
            <div class="title-font font-size22 text-theme-color">Discover our popular dishes</div>
            <h3 class="font-size24 text-uppercase">Our Menu</h3>
        </div>


        <div class="horizontaltab tab-style" style="display: block; width: 100%; margin: 0px;">
            <ul class="resp-tabs-list hor_1">
                <li class="resp-tab-item hor_1 resp-tab-active" aria-controls="hor_1_tab_item-0" role="tab">
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-01.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Special</span>
                </li>
                <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-1" role="tab">
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-02.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Breakfast</span>
                </li>
                <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-2" role="tab">
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-03.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Lunch</span>
                </li>
                <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-3" role="tab">
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-04.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Dinner</span>
                </li>

                <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-4" role="tab">
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-05.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Snacks</span>
                </li>
            </ul>
            <div class="resp-tabs-container hor_1">

                <h2 class="resp-accordion hor_1 resp-tab-active" role="tab" aria-controls="hor_1_tab_item-0"
                    style="background: none;"><span class="resp-arrow"></span>
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-01.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Special</span>
                </h2>
                <div class="resp-tab-content hor_1 resp-tab-content-active" aria-labelledby="hor_1_tab_item-0"
                     style="display:block">

                    <div class="row">
                        <?php foreach ($special as $key => $value) { ?>

                            <div class="col-md-6 margin-30px-bottom sm-margin-20px-bottom item-block-start"
                                 data-item-id="<?php echo $value['id'] ?>"
                                 data-restaurant-id="<?php echo $value['restaurant_id'] ?>"
                                 data-item-name="<?php echo $value['name'] ?>"
                                 data-item-price="<?php echo $value['price'] ?>">

                                <div class="border position-relative padding-20px-all bg-white padding-70px-right md-padding-60px-right">
                                    <div class="alt-font font-weight-700 margin-8px-bottom xs-margin-5px-bottom item-name">
                                        <?php echo $value['name'] ?>
                                    </div>

                                    <div class="title-font"> <?php echo $value['username'] ?></div>

                                    <p class="width-95 xs-width-100">
                                        <?php echo $value['description'] ?>
                                    </p>
                                    <div class="price-label bg-medium-light-gray text-theme-color alt-font font-weight-600">
                                        <span> ₹ <?php echo $value['price'] ?></span></div>
                                    <ul class="no-margin text-uppercase font-size12">
                                        <li>
                                            <span class="font-weight-700 alt-font">Ingredients</span>
                                            <span class="border-left padding-10px-left margin-10px-left"> <?php echo $value['ingredients'] ?></span>
                                        </li>
                                    </ul>

                                    <?php if ($role == 'customer') { ?>
                                        <div class="col-md-12 mt-25 pl-0">
                                            <a class="btn order-now">Order Now</a>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>

                        <?php } ?>
                    </div>

                </div>


                <h2 class="resp-accordion hor_1" role="tab" aria-controls="hor_1_tab_item-1">
                    <span class="resp-arrow"></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Breakfast</span>
                </h2>

                <div class="resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-1">
                    <div class="row">
                        <?php foreach ($breakfast as $key => $value) { ?>

                            <div class="col-md-6 margin-30px-bottom sm-margin-20px-bottom item-block-start"
                                 data-item-id="<?php echo $value['id'] ?>"
                                 data-restaurant-id="<?php echo $value['restaurant_id'] ?>"
                                 data-item-name="<?php echo $value['name'] ?>"
                                 data-item-price="<?php echo $value['price'] ?>">

                                <div class="border position-relative padding-20px-all bg-white padding-70px-right md-padding-60px-right">
                                    <div class="alt-font font-weight-700 margin-8px-bottom xs-margin-5px-bottom item-name">
                                        <?php echo $value['name'] ?>
                                    </div>

                                    <div class="title-font"> <?php echo $value['username'] ?></div>

                                    <p class="width-95 xs-width-100">
                                        <?php echo $value['description'] ?>
                                    </p>
                                    <div class="price-label bg-medium-light-gray text-theme-color alt-font font-weight-600">
                                        <span> ₹ <?php echo $value['price'] ?></span></div>
                                    <ul class="no-margin text-uppercase font-size12">
                                        <li>
                                            <span class="font-weight-700 alt-font">Ingredients</span>
                                            <span class="border-left padding-10px-left margin-10px-left"> <?php echo $value['ingredients'] ?></span>
                                        </li>
                                    </ul>

                                    <?php if ($role == 'customer') { ?>
                                        <div class="col-md-12 mt-25 pl-0">
                                            <a class="btn order-now">Order Now</a>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>

                        <?php } ?>
                    </div>
                </div>


                <h2 class="resp-accordion hor_1" role="tab" aria-controls="hor_1_tab_item-2"><span
                            class="resp-arrow"></span>
                    <span class="margin-8px-bottom display-inline-block xs-display-none"><img src="img/icon-tab-03.png"
                                                                                              alt=""></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Lunch</span>
                </h2>
                <div class="resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-2">

                    <div class="row">
                        <?php foreach ($lunch as $key => $value) { ?>

                            <div class="col-md-6 margin-30px-bottom sm-margin-20px-bottom item-block-start"
                                 data-item-id="<?php echo $value['id'] ?>"
                                 data-restaurant-id="<?php echo $value['restaurant_id'] ?>"
                                 data-item-name="<?php echo $value['name'] ?>"
                                 data-item-price="<?php echo $value['price'] ?>">

                                <div class="border position-relative padding-20px-all bg-white padding-70px-right md-padding-60px-right">
                                    <div class="alt-font font-weight-700 margin-8px-bottom xs-margin-5px-bottom item-name">
                                        <?php echo $value['name'] ?>
                                    </div>

                                    <div class="title-font"> <?php echo $value['username'] ?></div>

                                    <p class="width-95 xs-width-100">
                                        <?php echo $value['description'] ?>
                                    </p>
                                    <div class="price-label bg-medium-light-gray text-theme-color alt-font font-weight-600">
                                        <span> ₹ <?php echo $value['price'] ?></span></div>
                                    <ul class="no-margin text-uppercase font-size12">
                                        <li>
                                            <span class="font-weight-700 alt-font">Ingredients</span>
                                            <span class="border-left padding-10px-left margin-10px-left"> <?php echo $value['ingredients'] ?></span>
                                        </li>
                                    </ul>

                                    <?php if ($role == 'customer') { ?>
                                        <div class="col-md-12 mt-25 pl-0">
                                            <a class="btn order-now">Order Now</a>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>

                        <?php } ?>
                    </div>

                </div>
                <h2 class="resp-accordion hor_1" role="tab" aria-controls="hor_1_tab_item-3"><span
                            class="resp-arrow"></span>
                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Dinner</span>
                </h2>
                <div class="resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-3">

                    <div class="row">
                        <?php foreach ($dinner as $key => $value) { ?>

                            <div class="col-md-6 margin-30px-bottom sm-margin-20px-bottom item-block-start"
                                 data-item-id="<?php echo $value['id'] ?>"
                                 data-restaurant-id="<?php echo $value['restaurant_id'] ?>"
                                 data-item-name="<?php echo $value['name'] ?>"
                                 data-item-price="<?php echo $value['price'] ?>">

                                <div class="border position-relative padding-20px-all bg-white padding-70px-right md-padding-60px-right">
                                    <div class="alt-font font-weight-700 margin-8px-bottom xs-margin-5px-bottom item-name">
                                        <?php echo $value['name'] ?>
                                    </div>

                                    <div class="title-font"> <?php echo $value['username'] ?></div>

                                    <p class="width-95 xs-width-100">
                                        <?php echo $value['description'] ?>
                                    </p>
                                    <div class="price-label bg-medium-light-gray text-theme-color alt-font font-weight-600">
                                        <span> ₹ <?php echo $value['price'] ?></span></div>
                                    <ul class="no-margin text-uppercase font-size12">
                                        <li>
                                            <span class="font-weight-700 alt-font">Ingredients</span>
                                            <span class="border-left padding-10px-left margin-10px-left"> <?php echo $value['ingredients'] ?></span>
                                        </li>
                                    </ul>

                                    <?php if ($role == 'customer') { ?>
                                        <div class="col-md-12 mt-25 pl-0">
                                            <a class="btn order-now">Order Now</a>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>

                        <?php } ?>
                    </div>

                </div>


                <h2 class="resp-accordion hor_1" role="tab" aria-controls="hor_1_tab_item-4"><span
                            class="resp-arrow"></span>

                    <span class="display-block xs-display-inline-block text-uppercase alt-font font-weight-700">Snacks</span>
                </h2>
                <div class="resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-4">

                    <div class="row">
                        <?php foreach ($snacks as $key => $value) { ?>

                            <div class="col-md-6 margin-30px-bottom sm-margin-20px-bottom item-block-start"
                                 data-item-id="<?php echo $value['id'] ?>"
                                 data-restaurant-id="<?php echo $value['restaurant_id'] ?>"
                                 data-item-name="<?php echo $value['name'] ?>"
                                 data-item-price="<?php echo $value['price'] ?>">

                                <div class="border position-relative padding-20px-all bg-white padding-70px-right md-padding-60px-right">
                                    <div class="alt-font font-weight-700 margin-8px-bottom xs-margin-5px-bottom item-name">
                                        <?php echo $value['name'] ?>
                                    </div>

                                    <div class="title-font"> <?php echo $value['username'] ?></div>

                                    <p class="width-95 xs-width-100">
                                        <?php echo $value['description'] ?>
                                    </p>
                                    <div class="price-label bg-medium-light-gray text-theme-color alt-font font-weight-600">
                                        <span> ₹ <?php echo $value['price'] ?></span></div>
                                    <ul class="no-margin text-uppercase font-size12">
                                        <li>
                                            <span class="font-weight-700 alt-font">Ingredients</span>
                                            <span class="border-left padding-10px-left margin-10px-left"> <?php echo $value['ingredients'] ?></span>
                                        </li>
                                    </ul>

                                    <?php if ($role == 'customer') { ?>
                                        <div class="col-md-12 mt-25 pl-0">
                                            <a class="btn order-now">Order Now</a>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

</section>