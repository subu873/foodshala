<?php
require '../../conf/db.php';
require '../FoodShala.php'; // classFile
$foodShala = new FoodShala($conn);
$preference = '';
$name = mysqli_real_escape_string($conn, $_POST['name']);
$email = mysqli_real_escape_string($conn, $_POST['email']);
$password = mysqli_real_escape_string($conn, $_POST['password']);
$role = mysqli_real_escape_string($conn, $_POST['role']);
if (isset($_POST['preference'])) {
    $preference = mysqli_real_escape_string($conn, $_POST['preference']);
}


$result = $foodShala->register($name, $email, $password, $preference, $role);

if ($result) {
    $success = ["error" => false, "status" => 'successfully registered'];
    return print_r(json_encode($success));
} else {
    $fail = ["error" => true, "status" => 'registration failed'];
    return print_r(json_encode($fail));
}

?>