<?php
session_start();
$error = [];
require '../../conf/db.php'; // db config file
require '../FoodShala.php'; // Foodshala class file
$foodShala = new FoodShala($conn);
$user = null;

// checking user id and role as a restaurant
if (!isset($_SESSION['user_id']) && ($_SESSION['role']) != 'restaurant') {
    $error = ['error' => true, "message" => 'Auth issue'];
    return print_r(json_encode($error));
}

if (isset($_SESSION['user_id'])) {
    $user = $_SESSION['user_id'];
}

$role = $_SESSION['role'];
if ($role != 'restaurant') {
    $error = ["error" => true, "message" => 'You are not logined as a restaurant'];
    return print_r(json_encode($error));
}

$name = mysqli_real_escape_string($conn, $_POST['name']);
$desc = mysqli_real_escape_string($conn, $_POST['description']);
$type = mysqli_real_escape_string($conn, $_POST['type']); // dinner,lunch,special etc.
$foodPref = mysqli_real_escape_string($conn, $_POST['food_pref']); // veg or non veg
$ingredient = mysqli_real_escape_string($conn, $_POST['ingredient']);
$price = mysqli_real_escape_string($conn, $_POST['price']);

$result = $foodShala->addMenuItem($user, $name, $desc, $type, $foodPref, $ingredient, $price);
if ($result) {
    return print_r(json_encode(['error' => false, "message" => 'Successfully updated']));
} else {
    $error = ["error" => true, "message" => 'insert failed'];
    return print_r(json_encode($error));
}

?>