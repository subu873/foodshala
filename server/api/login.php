<?php
//session_start();
//include_once("db_connect.php");
//if (isset($_SESSION['user_id']) != "") {
//    header("Location: index.php");
//}
//if (isset($_POST['login'])) {
//    $email = mysqli_real_escape_string($conn, $_POST['email']);
//    $password = mysqli_real_escape_string($conn, $_POST['password']);
//    $result = mysqli_query($conn, "SELECT * FROM users WHERE email = '" . $email . "' and pass = '" . md5($password) . "'");
//    if ($row = mysqli_fetch_array($result)) {
//        $_SESSION['user_id'] = $row['uid'];
//        $_SESSION['user_name'] = $row['user'];
//        header("Location: index.php");
//    } else {
//        $error_message = "Incorrect Email or Password!!!";
//    }
//}
//?>


<?php
session_start();
require '../../conf/db.php';
require '../FoodShala.php'; // classFile
$foodShala = new FoodShala($conn);

$email = mysqli_real_escape_string($conn, $_POST['email']);
$password = mysqli_real_escape_string($conn, $_POST['password']);
$role = mysqli_real_escape_string($conn, $_POST['role']);

$result = $foodShala->login($email, $password, $role);
$user = [];

if ($result) {
    // starting session
    $_SESSION['user_id'] = $result['id'];
    $_SESSION['user_name'] = $result['username'];
    $_SESSION['email'] = $result['email'];
    $_SESSION['role'] = $result['role'];

    // storing user info in user array
    $user['name'] = $result['username'];
    $user['user_id'] = $result['id'];
    $user['email'] = $result['email'];
    $user['role'] = $result['role'];
    $success = ["error" => false, "status" => 'login success', "user" => $user];
    return print_r(json_encode($success));
} else {
    $fail = ["error" => true, "status" => 'login failed', "user" => $user];
    return print_r(json_encode($fail));
}

?>
