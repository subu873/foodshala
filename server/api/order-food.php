<?php
session_start();
$error = [];
require '../../conf/db.php'; // db config file
require '../FoodShala.php'; // Foodshala class file
$foodShala = new FoodShala($conn);
$user = null;

if (!isset($_SESSION['user_id'])) {
    $error = ["error" => true, "message" => 'auth issue'];
    return print_r(json_encode($error));
}

$user = $_SESSION['user_id'];

$restaurantId = $_POST['restaurant'];
$itemName = mysqli_real_escape_string($conn, $_POST['item_name']);
$itemPrice = $_POST['item_price'];
$itemId = $_POST['item_id'];

$itemExist = $foodShala->checkMenuItemExist($restaurantId, $itemId);
if ($itemExist) {
    $priceCheckforItem = $foodShala->checkMenuNameByMenuId($itemId, $itemName, $itemPrice);
    if ($priceCheckforItem) {
        $orderStatus = $foodShala->orderFood($user, $restaurantId, $itemId, $itemName, $itemPrice);
        if ($orderStatus) {
            $success = ["error" => false, "message" => 'ordered succesfully'];
            return print_r(json_encode($success));
        } else {
            $error = ["error" => true, "message" => 'order failed'];
        }
    } else {
        $error = ['error' => true, 'message' => 'Item price not matched with item name and item Id'];
        return print_r(json_encode($error));
    }

} else {
    $error = ['error' => true, 'message' => 'Item not found for this restaurant'];
    return print_r(json_encode($error));
}