<?php


class FoodShala
{

    private $conn;
    private $user;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function login($email, $password, $role)
    {
        if (empty($role)) {
            echo "Please Provide Role First for login";
            return false;
        }

        if ($role == 'customer') {
            return $this->loginCustomer($email, $password);
        } else {

            return $this->loginRestaurant($email, $password);
        }

    }

    public function loginCustomer($email, $password)
    {

        $sql = "SELECT * FROM `users` WHERE email='$email' AND password=md5('$password') AND role='customer'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) == 1) {
            $row = mysqli_fetch_assoc($result);
            return $row;
        } else {
            return false;
        }
    }

    public function loginRestaurant($email, $password)
    {
        $sql = "SELECT * FROM `users` WHERE email='$email' AND password=md5('$password') AND role='restaurant'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) == 1) {
            $row = mysqli_fetch_assoc($result);
            return $row;
        } else {
            return false;
        }

    }

    public function register($name, $email, $password, $preference, $role)
    {

        if ($role == 'customer') {
            // for customer role registration
            return $this->registerCustomer($name, $email, $password, $preference);
        } else {
            //for Restaurant role registarion
            return $this->registerRestaurant($name, $email, $password);
        }

    }

    public function registerCustomer($name, $email, $password, $preference)
    {
        $role = 'customer';
        $sql = "INSERT INTO `users`(`username`, `password`, `email`, `role`, `preference`, `created_at`) VALUES ('$name','" . md5($password) . "','$email','$role','$preference',current_timestamp())";
        $result = mysqli_query($this->conn, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function registerRestaurant($name, $email, $password)
    {
        $role = 'restaurant';
        $sql = "INSERT INTO `users`(`username`, `password`, `email`, `role`, `created_at`) VALUES ('$name','" . md5($password) . "','$email','$role', current_timestamp())";
        $result = mysqli_query($this->conn, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function addMenuItem($resId, $name, $desc, $type, $foodPref, $ingredient, $price)
    {
        $sql = "INSERT INTO `menu`(`restaurant_id`, `name`, `description`, `type`,`food_preference`, `ingredients`, `price`, `created_at`) VALUES ('$resId', '$name', '$desc', '$type','$foodPref', '$ingredient','$price',current_timestamp())";
        //var_dump($sql);
        $result = mysqli_query($this->conn, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    public function getMenuList($type, $resId)
    {
        $items = [];
        $sql = "SELECT * from `menu` WHERE type='$type' AND `restaurant_id`='$resId'";
        //var_dump($sql);
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($items, $row);
            }
        }
        return $items;
    }

    public function getMenuListWithRestaurantName($type)
    {
        $items = [];
        $sql = "SELECT menu.*,users.username from `menu` LEFT JOIN `users` ON menu.restaurant_id=users.id WHERE `type`='$type'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                array_push($items, $row);
            }
        }
        return $items;
    }

    public function orderFood($user, $resId, $itemId, $itemName, $itemPrice)
    {

        $sql = "INSERT INTO `orders`(`user_id`, `restaurant_id`, `menu_id`, `item_name`, `item_price`, `created_at`) VALUES ('$user','$resId','$itemId','$itemName','$itemPrice',current_timestamp())";
        $result = mysqli_query($this->conn, $sql);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function checkMenuItemExist($resId, $menuId)
    {
        $sql = "SELECT * from `menu` WHERE id=$menuId AND `restaurant_id`=$resId";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function checkMenuNameByMenuId($menuId, $menuName, $price)
    {
        $sql = "SELECT * from `menu` WHERE id='$menuId' AND `name`= '$menuName' AND `price`=$price";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) == 1) {
            return true;
        } else {
            return false;
        }
    }
}