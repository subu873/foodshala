<header class="header-style8 fixedHeader">

    <!-- start navbar -->
    <nav class="navbar navbar-expand-lg alt-font nav-scroll">
        <div class="container sm-padding-10px-tb sm-padding-15px-lr">

            <!-- start logo -->
            <a class="logo" href="javascript:void(0);" data-scroll-nav="0">
                Food<span>Shala</span>
            </a>
            <!-- end Logo -->

            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarContent"
                    aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"></span>
            </button>

            <!-- navbar links -->
            <div class="collapse navbar-collapse" id="navbarContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link active" href="<?php echo $webroot ?>">Home</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);">Menu</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target="#loginModal">Customer Login</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target="#loginModalRestaurant">Restaurant Login</a>
                    </li>
                    <?php if(isset($_SESSION['user_id'])){?>
                    <li class="nav-item">
                        <a class="nav-link" id="logout">Logout</a>
                    </li>
                    <?php } ?>
                    <li class="nav-item">
                        <a class="nav-link btn theme small" href="javascript:void(0);" data-scroll-nav="4">Book a
                            Table</a>
                    </li>
                </ul>
            </div>
            <!-- end navbar links -->

        </div>
        <a href="javascript:void(0)" class="sidemenu_btn sm-display-none" id="sidebar_toggle">
            <span></span> <span></span> <span></span>
        </a>
    </nav>
    <!-- end navbar  -->

    <div class="container-fluid">
        <div class="row">
            <div class="owl-carousel owl-theme width-100">
                <div class="text-center item bg-img" data-overlay-dark="7" data-background="img/bg1.jpg">
                    <div class="absolute-middle-center caption">
                        <div class="overflow-hidden">
                            <h3 class="title-font text-theme-color font-size28 sm-font-size18 no-margin">Welcome to</h3>
                            <h1 class="banner-headline clip text-white">Restaurant &amp; Cafe</h1>
                            <p class="margin-30px-bottom sm-margin-20px-bottom xs-display-none text-white">Tasty,
                                delicious food ready in restaurant. We respect your test review.</p>
                            <a href="javascript:void(0);" data-scroll-nav="2" class="btn theme">Learn More</a>
                        </div>
                    </div>
                </div>
                <div class="text-center item bg-img" data-overlay-dark="8" data-background="img/bg2.jpg">
                    <div class="absolute-middle-center caption">
                        <div class="overflow-hidden">
                            <h3 class="title-font text-theme-color font-size28 sm-font-size18 no-margin">Food with</h3>
                            <h1 class="banner-headline clip text-white">Endless Possibilities</h1>
                            <p class="margin-30px-bottom sm-margin-20px-bottom xs-display-none text-white">Tasty,
                                delicious food ready in restaurant. We respect your test review.</p>
                            <a href="javascript:void(0);" data-scroll-nav="2" class="btn theme">Learn More</a>
                        </div>
                    </div>
                </div>
                <div class="text-center item bg-img" data-overlay-dark="8" data-background="img/bg3.jpg">
                    <div class="absolute-middle-center caption">
                        <div class="overflow-hidden">
                            <h3 class="title-font text-theme-color font-size28 sm-font-size18 no-margin">We provide</h3>
                            <h1 class="banner-headline clip text-white">Delicious Foods Zone</h1>
                            <p class="margin-30px-bottom sm-margin-20px-bottom xs-display-none text-white">Tasty,
                                delicious food ready in restaurant. We respect your test review.</p>
                            <a href="javascript:void(0);" data-scroll-nav="2" class="btn theme">Learn More</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- start sidemenu -->
    <div class="side-menu black alt-font">
        <div class="inner-wrapper">
            <span class="btn-close" id="btn_sidebar_colse"></span>
            <nav class="side-nav width-100">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link active" href="javascript:void(0);" data-scroll-nav="0">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="1">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="2">Menu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="3">Our Chef</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="5">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target="#loginModal">Customer Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0);" data-scroll-nav="7">Contact</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo $webroot?>/server/api/logout.php">Logout</a>
                    </li>



                </ul>
            </nav>

            <div class="side-footer width-100">
                <div class="social-links">
                    <a href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a>
                    <a href="javascript:void(0);"><i class="fab fa-twitter"></i></a>
                    <a href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- End sidemenu -->

    <!-- close sidemenu -->
    <a id="close_sidebar" href="javascript:void(0);"></a>
    <!-- End Sidemenu -->

    <div class="arrow">
        <a href="javascript:void(0);" data-scroll-nav="1">
            <i class="fas fa-chevron-down"></i>
        </a>
    </div>

</header>

<?php if (!isset($_SESSION['user_id']) && !isset($_SESSION['role'])) { ?>
    <?php include 'login-customer.php' ?>
    <?php include 'login-restaurant.php' ?>
<?php } ?>
