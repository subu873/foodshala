<!-- Modal -->
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Customer Login</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <form role="form" method="post" onsubmit="return false"
                      name="loginCustomer">

                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" name="email" placeholder="Email" id="customerEmail">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="password" name="password" placeholder="Password" id="customerPassword">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <span class="text-danger font-weight-700" id="errorLoginMsgCustomer"></span>
                    </div>

                    <div class="col-md-12" style="margin-top: 15px">
                        <button class="butn theme medium" id="loginCustomer"><span>Login</span>
                        </button>
                        <p class="no-margin float-right"><a href="register-customer.php">No Account ? Register here</a>
                        </p>
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript" src="<?php echo $webroot ?>/assets/js/login.js"></script>
