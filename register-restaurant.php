<?php require 'conf/config.php'; ?>


<!DOCTYPE html>
<html lang="en">

<head>

    <!-- metas -->
    <meta charset="utf-8">
    <meta name="author" content="ChitrakootWeb"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="Onepage Restaurant and Cafe Template"/>
    <meta name="description" content="Onepage Restaurant and Cafe Template"/>

    <!-- title  -->
    <title>FoodShala</title>

    <!-- favicon -->
    <link rel="shortcut icon" href="<?php echo $webroot ?>/template/img/favicon.png">
    <link rel="apple-touch-icon" href="<?php echo $webroot ?>/template/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $webroot ?>/template/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo $webroot ?>/template/img/apple-touch-icon-114x114.png">

    <!-- plugins -->
    <link rel="stylesheet" href="<?php echo $webroot ?>/template/css/plugins.css"/>

    <!-- core style css -->
    <link rel="stylesheet" href="<?php echo $webroot ?>/template/css/style.css"/>

    <script type="text/javascript" src="<?php echo $webroot ?>/assets/js/config.js"></script>

</head>

<body>

<!-- start page loading -->
<div id="preloader">
    <div class="row loader">
        <div class="loader-icon"></div>
    </div>
</div>
<!-- end page loading -->

<!-- start header -->
<?php require 'layout/header.php' ?>

<section class="page-title-section2 bg-img cover-background" data-overlay-dark="0"
         data-background="https://s3-ap-southeast-1.amazonaws.com/internshala-uploads/banner-images/home/main-1366.png"
         style="background-image: url('https://s3-ap-southeast-1.amazonaws.com/internshala-uploads/banner-images/home/main-1366.png');">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Restaurant Registration</h1>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 center-col">
                <div class="bg-white padding-30px-all sm-padding-20px-all border border-width-5">
                    <div class="section-heading">
                        <h3>Restaurant Registration</h3>

                    </div>
                    <form class="registration-form" method="post" name="register" onsubmit="return false">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" name="username" id="name" required="required"
                                       placeholder="User Name or Restaurant Name *" maxlength="70" class="medium-input">
                            </div>

                            <div class="col-12">
                                <input type="email" name="email" id="email" required="required" placeholder="Email *"
                                       maxlength="70" class="medium-input">
                            </div>

                            <div class="col-12">
                                <input type="password" name="password" id="password" required="required"
                                       placeholder="Password *" maxlength="70" class="medium-input">
                            </div>
                            <div class="col-12">
                                <input type="password" name="confirmpassword" id="confirmpassword" required="required"
                                       placeholder="Confirm Password *" maxlength="70" class="medium-input">
                            </div>

                            <div class="col-md-12">
                                <span class="text-danger font-weight-700" id="errorMsg"></span>
                                <span class="text-success font-weight-700" id="successMsg"></span>
                            </div>

                            <div class="col-12 " style="margin-top: 15px">
                                <button class="butn theme medium" id="registerRestaurant"><span>Registration</span>
                                </button>
                                <p class="no-margin float-right"><a href="login-customer.php">Already Register?</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>

<?php require 'layout/footer.php' ?>


<!-- jQuery -->
<script src="<?php echo $webroot ?>/template/js/jquery.min.js"></script>

<!-- popper.min -->
<script src="<?php echo $webroot ?>/template/js/popper.min.js"></script>

<!-- bootstrap -->
<script src="<?php echo $webroot ?>/template/js/bootstrap.min.js"></script>

<!-- scrollIt -->
<script src="<?php echo $webroot ?>/template/js/scrollIt.min.js"></script>

<!-- stellar js -->
<script src="<?php echo $webroot ?>/template/js/jquery.stellar.min.js"></script>

<!-- isotope.pkgd.min js -->
<script src="<?php echo $webroot ?>/template/js/isotope.pkgd.min.js"></script>


<!-- custom scripts -->
<script src="<?php echo $webroot ?>/template/js/main.js"></script>

<script type="text/javascript" src="<?php echo $webroot ?>/assets/js/register.js"></script>



