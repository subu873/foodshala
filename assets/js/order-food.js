$('.order-now').click(function () {
    openLoginModalCustomer();
    let elem = $(this).closest('.item-block-start');
    let resId = elem.attr('data-restaurant-id');
    let item_name = elem.attr('data-item-name');
    let item_price = elem.attr('data-item-price');
    let itemId = elem.attr('data-item-id');

    let orderJson = {restaurant: resId, item_name: item_name, item_price: item_price, item_id: itemId};

    orderFoodAjax(orderJson);

});

function openLoginModalCustomer() {
    if (user != '') return;
    $('#loginModal').modal('show');
}

function orderFoodAjax(orderData) {

    $.ajax({
        type: 'POST',
        url: HOST + '/server/api/order-food.php',
        data: orderData,
        success: function (data) {
            console.log(data);
        },
        error: function (data) {
            console.log(data);
        }
    })

}
