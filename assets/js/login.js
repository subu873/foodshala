/*
* Register as a customer ajax call
* */


$('#loginCustomer').click(function () {

    let email = $('#customerEmail').val().trim();
    let password = $('#customerPassword').val().trim();
    let role = 'customer';

    //hide error Msg
    $('#errorLoginMsgCustomer').hide();

    if (email == '') {
        $('#errorLoginMsgCustomer').text('Please type email id').show();
        return false;
    }

    if (password == '') {
        $('#errorLoginMsgCustomer').text('Please type password').show();
        return false;
    }

    loginAjax(email, password, role);

});


function loginAjax(email, password, role) {
    let errorMsgId = (role == 'customer') ? '#errorLoginMsgCustomer' : '#errorLoginMsgRestaurant';
    $.ajax({
        type: 'POST',
        url: HOST + '/server/api/login.php',
        data: {email: email, password: password, role: role},
        success: function (data) {
            let res = JSON.parse(data);
            if (res.error == true) {
                $(errorMsgId).text('Email and Password not matched').show();
            } else {
                console.log('Login Success', res);
                localStorage.setItem('username', res.user.name);
                localStorage.setItem('userId', res.user.user_id);
                localStorage.setItem('email', res.user.email);
                localStorage.setItem('role', res.user.role);
                if (res.user.role == 'customer') {
                    window.location.href = HOST + '/admin/';
                } else {
                    window.location.href = HOST + '/admin/restaurant/restaurant-admin.php';
                }

            }
        },
        error: function (data) {
            console.log(data);
        }
    })
}


$('#loginRestaurant').click(function () {

    let email = $('#restaurantEmail').val().trim();
    let password = $('#restaurantPassword').val().trim();
    let role = 'restaurant';

    //hide error Msg
    $('#errorLoginMsgRestaurant').hide();

    if (email == '') {
        $('#errorLoginMsgRestaurant').text('Please type email id').show();
        return false;
    }

    if (password == '') {
        $('#errorLoginMsgRestaurant').text('Please type password').show();
        return false;
    }

    loginAjax(email, password, role);

});
