$('#addItem').click(function () {
    let name = $('#name').val();
    let desc = $('#description').val();
    let vegText = $('#type').val(); // veg or non veg
    let foodCat = $('#foodCat').val(); // dinner,lunch etc
    let ingredient = $('#ingredients').val();
    let price = $('#price').val();

    $('#successItemMsg').text('').hide();
    $('#errorItemMsg').text('').hide();

    if (name == '' || desc == '' || vegText == '' || foodCat == '' || ingredient == '' || price == '') {
        $('#errorItemMsg').text('All fields are required').show();
        return false;
    }

    addItemJson = {
        name: name,
        description: desc,
        type: foodCat,
        ingredient: ingredient,
        food_pref: vegText,
        price: price
    };

    addMenuItem(addItemJson);

});

function addMenuItem(formdata) {

    $.ajax({
        type: 'POST',
        url: HOST + '/server/api/add-menu-item.php',
        data: formdata,
        success: function (data) {
            console.log(data);
            let res = JSON.parse(data);
            if (res.error == 'true') {
                console.log('Update Failed');
            } else {
                console.log('Success');
                $('#successItemMsg').text('✔✔ Item Succesfully Added in Menu').show();
            }
        },
        error: function (data) {
            console.log(data);

        }
    })

}