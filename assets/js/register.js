/*
* Register as a customer ajax call
* */


$('#registerCustomer').click(function () {
    let name = $('#name').val();
    let email = $('#email').val();
    let pass = $('#password').val();
    let cPass = $('#confirmpassword').val();
    let role = 'customer';
    let preference = $('#preference').val();

    //hide error and success mesage when click
    $('#successMsg').hide();
    $('#errorMsg').hide();

    if (name == '' || email == '' || pass == '' || preference == '') {
        $('#errorMsg').text('*All Fields are required').show();
        return false;
    }

    if (pass != cPass) {
        $('#errorMsg').text('Password not matched').show();
        return false;
    }
    let registerFormData = {name: name, email: email, password: pass, role: role, preference: preference};
    console.log('register data', registerFormData);
    registerAjax(registerFormData);

});


function registerAjax(formData) {
    $.ajax({
        type: 'POST',
        url: HOST + '/server/api/register.php',
        data: formData,
        success: function (data) {
            console.log(data);
            let res = JSON.parse(data);
            if (res.error == true) {
                $('#errorMsg').text('Something went wrong').show();
            } else {
                $('#successMsg').text('Successfully Registered !!').show();
            }

        },
        error: function (data) {
            console.log(data);
        }
    })
}

$('#registerRestaurant').click(function () {
    let name = $('#name').val().trim();
    let email = $('#email').val().trim();
    let pass = $('#password').val().trim();
    let cPass = $('#confirmpassword').val().trim();
    let role = 'restaurant';

    //hide error and success mesage when click
    $('#successMsg').hide();
    $('#errorMsg').hide();

    if (name == '' || email == '' || pass == '') {
        $('#errorMsg').text('*All Fields are required').show();
        return false;
    }

    if (pass != cPass) {
        $('#errorMsg').text('Password not matched').show();
        return false;
    }
    let registerFormData = {name: name, email: email, password: pass, role: role};
    registerAjax(registerFormData);
});